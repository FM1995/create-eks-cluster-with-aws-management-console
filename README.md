# Create EKS cluster with AWS Management Console

## Table of Contents
- [Create EKS IAM Role](#create-eks-iam-role)
- [Create VPC for Worker Nodes](#create-vpc-for-worker-nodes)
- [Create EKS Cluster Control Plane Nodes](#create-eks-cluster-control-plane-nodes)
- [Connect to EKS Cluster Locally with kubectl](#connect-to-eks-cluster-locally-with-kubectl)
- [Create EC2 IAM Role for Node Group](#create-ec2-iam-role-for-our-node-group)


#### Project Outline

To create an Amazon EKS cluster, start by configuring IAM roles for EKS and worker nodes. Set up a VPC for worker nodes and deploy the EKS control plane. Connect kubectl to the cluster, and create IAM roles for worker nodes. Finally, form a Node Group and attach it to the EKS cluster for running containerized applications. These steps establish roles, permissions, and infrastructure for efficient EKS cluster management.


## Lets get started

#### Create EKS IAM Role


Create EKS IAM role to so that EKS can create resources

We will create a role specific to our EKS cluster and therefore configure and create resources

Lets navigate to IAM -> Roles -> Create Role

![Image1](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image1.png)


Can see the ec2 permissions

![Image2](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image2.png)

Now giving it a name

![Image3](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image3.png)

The eks role got created

![Image4](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image4.png)

#### Create VPC for Worker Nodes

Now that we have the role created we can create the VPC for the cluster to run in
Despite having a default VPC, EKS cluster needs specific networking configuration, K8s and AWS have specific networking rules and the default VPC is not configured for it therefore we need to create a  customized VPC. This will be specific to the worker nodes which are the EC2 isntances, since they have configurations like NACLS and Security groups we would need to specify firewall configurations. We also have the control plane which is in another vpc which connected to our our VPC and AWS account via an api.
Some of the best practices include public and private subnets, this includes configurations like Kubernetes load balancer service for the private subnet and elastic load balancer for the public subnet
Now how do we create the VPC with EKS specific configurations
Instead of creating the VPC from scratch we can use a template from Cloudformation
Using the below link


https://docs.aws.amazon.com/eks/latest/userguide/creating-a-vpc.html

Using the below

![Image5](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image5.png)

Can then navigate to the URL’s and download the contents as seen below and can see the IP addresses that gets assigned

![Image6](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image6.png)

Can then create the stack using the pre-defined template

![Image7](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image7.png)

And see it has been created

![Image8](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image8.png)

Now we can see the outputs which has all the parameters needed for our EKS cluster

![Image9](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image9.png)

Now that we have our EKS VPC we can now create our EKS cluster


#### Create EKS cluster control plane nodes

Lets create the EKS cluster using the following

![Image10](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image10.png)

Now we are ready to create the cluster locally

#### Connect to EKS cluster locally with kubectl 

Now that we have our cluster created

![Image11](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image11.png)

Lets connect to our cluster via the terminal

Lets see our region

![Image12](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image12.png)

Now to connect our cluster using the below

```
aws eks update-kubeconfig --name eks-cluster-test-2024
```

![Image13](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image13.png)

Then running

```
kubectl cluster-info
```

Can see it is connected through the API endpoint

![Image14](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image14.png)

Now lets create worker nodes

Since we are using ec2 instances we will be using node group and nodes

Node groups are practical so that we can manage nodes efficiently depending on the demand

#### Create EC2 IAM Role for Node Group

Now lets create node groups and attach it to our EKS cluster
Since the nodes run worker processes, Kubelet is the mina process for scheduling, managing pods and communicate with other AWS Services
Kubelet on worker node which is on the ec2 instance needs permissions to perform certain actions and therefore we need a role for our Node group
Lets navigate back to IAM service
And assign the AmazonEKSWorkerNodePolicy


![Image15](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image15.png)

Can also assign 

AmazonEC2ContainerRegistryReadOnly

![Image16](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image16.png)

Can also assign EKS CNI, this is the network interface so that pods can communicate with each other

![Image17](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image17.png)

Can then create the role

![Image18](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image18.png)

Now lets add the node group to the EKS cluster

Can create the node using the compute configuration

![Image19](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image19.png)

Can see the nodes are ready

![Image20](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image20.png)

Now checking via the command line

Now the node group is connected to the eks cluster

And now we can start creating pods, container and so on

![Image21](https://gitlab.com/FM1995/create-eks-cluster-with-aws-management-console/-/raw/main/Images/Image21.png)







